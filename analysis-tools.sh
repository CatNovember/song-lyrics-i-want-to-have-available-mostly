#!/bin/sh
# Spits out numbers, statistics, helpful things about songlyrics.org

filename=songlyrics.org

# Artists
printf '\n'
grep "^\*\*\ " "${filename}" | sed 's/^** //' | tr '\n' ' '
printf '\n'

# Number of artists
printf '\n'
printf '%s' "^ ~" && grep -c "^\*\*\ " "${filename}" && printf '%s\n' "artists"
printf '%s' "       (Approximate. Searchable. Language -> Alphabetical)"
printf '\n\n'

# Number of songs
printf '%s' "~" && grep -c "^\*\*" "${filename}"
printf '%s' "songs"
printf '\n\n'

# Number of languages
grep -c "^\*\ " "${filename}" && printf '%s' "languages:"
printf '\n\n'

# Languages
    grep "^\*\ " "${filename}" | # Top level headings
    grep -v "/ /"              | # Remove language lines without difficult characters
    sed 's/^\* //'             | # Remove marker
    sed 's/,/, /g'               # Space after comma
printf '\n'

# Lines
wc -l "${filename}" | awk '{ print $1 }'
printf '%s\n\n' "lines"

# Words
wc -w "${filename}" | awk '{ print $1 }'
printf '%s\n\n' "words"

# Longest lines
printf '%s' "10 highest line lengths in # of chars: " && \
    awk '{ print length }' "${filename}"  | # Length of lines
    sort -n                               | # Numeric sort
    tail                                  | # 10 biggest only
    tac                                   | # Biggest number first
    tr '\n' ','                           | # Newlines -> commas
    sed 's/,/, /g'                        | # Space after comma
    rev | cut -c3- | rev                    # Trailing comma removed
printf '\n'

# Disk usage
du -h "${filename}" | awk '{ print $1 }'
printf '%s\n\n' "Disk usage"

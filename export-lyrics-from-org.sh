#!/bin/sh
# Exports e.g. songlyrics from org to tex, PDF and HTML

input_file=songlyrics.org

book_type=songlyrics
project_name=catnovember

# Grey PDFs
cat \
        header-files/latex-font-and-colour-1.org \
        header-files/grey.org                    \
        header-files/latex-colour-2.org          \
        header-files/title-date-author.org       \
        "${input_file}"                          \
               > grey-title-input.org            \
        && echo "grey-title-input.org created" && echo ""

## A3
cat \
        header-files/mode.org                                              \
        header-files/A3.org                                                \
        grey-title-input.org                                               \
               > output-files/"${book_type}"-A3-grey-"${project_name}".org \
        && echo "output-files/${book_type}-A3-grey-${project_name}.org created" && echo ""
## A4
cat \
        header-files/mode.org                                              \
        header-files/A4.org                                                \
        grey-title-input.org                                               \
               > output-files/"${book_type}"-A4-grey-"${project_name}".org \
        && echo "output-files/${book_type}-A4-grey-${project_name}.org created" && echo ""
## A5
cat \
        header-files/mode.org                                              \
        header-files/A5.org                                                \
        grey-title-input.org                                               \
               > output-files/"${book_type}"-A5-grey-"${project_name}".org \
        && echo "output-files/${book_type}-A5-grey-${project_name}.org created" && echo ""
## A6
cat \
        header-files/mode.org                                              \
        header-files/A6.org                                                \
        grey-title-input.org                                               \
               > output-files/"${book_type}"-A6-grey-"${project_name}".org \
        && echo "output-files/${book_type}-A6-grey-${project_name}.org created" && echo ""

# Light PDF
cat \
        header-files/latex-font-and-colour-1.org \
        header-files/light.org                   \
        header-files/latex-colour-2.org          \
        header-files/title-date-author.org       \
        "${input_file}"                          \
               > light-title-input.org           \
        && echo "light-title-input.org created" && echo ""

## A4
cat \
        header-files/mode.org                                               \
        header-files/A4.org                                                 \
        light-title-input.org                                               \
               > output-files/"${book_type}"-A4-light-"${project_name}".org \
        && echo "output-files/${book_type}-A4-light-${project_name}.org created" && echo ""
## A5
cat \
        header-files/mode.org                                               \
        header-files/A5.org                                                 \
        light-title-input.org                                               \
               > output-files/"${book_type}"-A5-light-"${project_name}".org \
        && echo "output-files/${book_type}-A5-light-${project_name}.org created" && echo ""
## A6
cat \
        header-files/mode.org                                               \
        header-files/A6.org                                                 \
        light-title-input.org                                               \
               > output-files/"${book_type}"-A6-light-"${project_name}".org \
        && echo "output-files/${book_type}-A6-light-${project_name}.org created" && echo ""

# HTML
## Grey
cat \
              header-files/mode.org                                                                                 \
              header-files/HTML.org                                                                                 \
              header-files/title-date-author.org                                                                    \
              "${input_file}"                                                                                       \
                         > "${book_type}"-HTML-grey-"${project_name}"-with-comments.org                             \
                   && echo "${book_type}"-HTML-grey-"${project_name}"-with-comments.org created && echo ""          \
                                                                                                                    \
&& grep -v "BEGIN_COMMENT" "${book_type}"-HTML-grey-"${project_name}"-with-comments.org |                           \ # HTML can handle UTF-8 well enough already
   grep -v "END_COMMENT" | grep -v "/ " > "${book_type}"-HTML-grey-"${project_name}".org                            \
                                                                                                                    \
                  && emacs "${book_type}"-HTML-grey-"${project_name}".org --batch -f org-html-export-to-html --kill \
       && echo "" && echo  "${book_type}"-HTML-grey-"${project_name}".html created && echo ""

# Print PDF
## A4
cat \
        header-files/mode.org                                               \
        header-files/A4.org                                                 \
        header-files/print.org                                              \
        header-files/title-date-author.org                                  \
        "${input_file}"                                                     \
               > output-files/"${book_type}"-A4-print-"${project_name}".org \
        && echo "output-files/${book_type}-A4-print-${project_name}.org created" && echo ""

for orgfiles in output-files/*.org; do
        [ -e "${orgfiles}" ] || continue
        emacs "${orgfiles}" --batch -f org-latex-export-to-pdf --kill
        echo ""
done && echo "PDFs created" && echo ""

# Cleanup
mv output-files/*.html output-files/html/ && echo "html cleaned"
mv output-files/*.org  output-files/org/  && echo "org  cleaned"
mv output-files/*.pdf  output-files/pdf/  && echo "pdf  cleaned"
mv output-files/*.tex  output-files/tex/  && echo "tex  cleaned"

mv grey-title-input.org output-files/org/  && echo "grey-title-input.org  cleaned"
mv light-title-input.org output-files/org/ && echo "light-title-input.org cleaned"

# Cleanup
mv ./*html output-files/html/         && mv output-files/*.html output-files/html/ && echo "html cleaned"
mv songlyrics-*.org output-files/org/ && mv output-files/*.org  output-files/org/  && echo "org  cleaned"
rm ./*.html~

# Download
| File                                                                                                                                                                                     | Size   |
| -                                                                                                                                                                                        | -:     |
| [A3-grey       ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-A3-grey-catnovember.pdf?inline=false)               | 2.5 MB |
| [A4-grey       ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-A4-grey-catnovember.pdf?inline=false)               | 2.5 MB |
| [A4-light      ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-A4-light-catnovember.pdf?inline=false)              | 2.5 MB |
| [A4-print      ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-A4-print-catnovember.pdf?inline=false)              | 2.4 MB |
| [A5-grey       ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-A5-grey-catnovember.pdf?inline=false)               | 2.9 MB |
| [A5-light      ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-A5-light-catnovember.pdf?inline=false)              | 2.9 MB |
| [A6-grey       ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-A6-grey-catnovember.pdf?inline=false) (recommended) | 3.4 MB |
| [A6-light      ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-A6-light-catnovember.pdf?inline=false)              | 3.5 MB |
| [HTML-grey-HTML](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/html/songlyrics-HTML-grey-catnovember.html)                        | 3.4 MB |
| [HTML-grey-PDF ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/output-files/pdf/songlyrics-HTML-grey-catnovember.pdf?inline=false)             | 2.8 MB |
| [Org-file      ](https://gitlab.com/CatNovember/song-lyrics-i-want-to-have-available-mostly/-/raw/master/songlyrics.org)                                                                 | 2.5 MB |

# Song lyrics I want to have available. Mostly

I like singing. There are many songs. I don't know all the lyrics. Singing together is nice.

# Software

http://spacemacs.org using org-mode

The org-file is what I edit. It's plain text. Org-mode in (Spac)emacs makes it a breeze

Default export to PDF via pdftex(?) now. See "future"

# Making your own

In the beginning: It's a lot. You've heard *many, many* songs. But it gets better

- Have a separate todo.org-file, with artists, individual songs, playlists, albums, what have you
- Keywordsearch for lyrics-sites. So you only have to type in "l Artist Name" into your web browser's address bar, and it searches your favourite lyrics site
- https://lyrics.fandom.com/ has better spacing, https://genius.com has a larger selection, https://azlyrics.com was disappointing to use
- To find the popular songs by an artist, I would have their lyrics.fandom.com-site in the 1st browser tab, a YouTube-search for them in the 2nd, and then open songs I recognised the names of from that YouTube tab, in new tabs (#3, #4, etc.). When I finished opening YouTube-tabs, I would leave that YouTube-search tab open, and go through #3, #4 etc. Pressing 1 gets you to 10% of a video, pressing 2 gets you to 20%, etc. When I knew I didn't want to include that song: Ctrl+W to close that tab, and get to the next one. When I knew I *did* want it: Ctrl+W to close that tab, K to pause the next video, Ctrl+1 to get to tab #1 (the lyrics.fandom.com-tab), Ctrl+F to search for that song's name, and then open that in a new tab. When I had gone through all I thought I would want from that artist: Close the YouTube-tabs, make a new entry in the org-file for that artist, and start copying and pasting the lyrics into their respective entries
- Perfectionism is death. You'll never get all songs. You'll never get them all exactly correct. Having 1000 songs that are at 98%, is a lot better than having 10 songs that are 100%. Other people and I have had the thought of genres: Tell me an easy way to implement it, and I will. But agonising over every single song's genre? No thank you. Life's too short.

# Future

- YouTube-search link next to every song(?) - Searching for that title. Should be a simple sed-command or something, used on a copy of the org-file, which is then exported.
- A functional song-counting script, number added to filenames. At the moment, I `grep "^\*\*\*" songlyrics.org | wc -l` which is not super accurate
- Better explanation of how to keywordsearch
- Researched gitlab.com's robots.txt file

# Right to copy

I know about copyright. I'm not going to pretend not to. And "ignorance of the law, […]" etc.

I don't intend on sharing this super widely, though of course I can't stop other people from doing so. This is not an abnegation of responsibility. Just to explain my intention. This is meant for myself, and people I come into contact with.

You can see I haven't blindly pasted in all of [Famous Artist with hundreds of songs]'s lyrics here, pretending to want to sing every one of them, while sneakily trying to make a massive database available to the world.

I'm trying a robots.txt file on GitLab.com for now, but am still somewhat unfamiliar with the platform.

Thanks for understanding, I know many of your hands are tied and it's not worth your job, and that some of you are robots. Hi robots.

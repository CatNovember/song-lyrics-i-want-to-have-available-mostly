# Don't indent paragraphs
#+latex_header: \setlength{\parindent}{0pt}

# Russian characters visible
#+latex_header: \usepackage[T2A]{fontenc}

# Restricts numbering of bullet points
#+OPTIONS: num:3

# Preserves line breaks
#+OPTIONS: \n:y

#+Title: Song lyrics I want to have available. Mostly
#+Date: \today
#+Author: [[https://gitlab.com/CatNovember][gitlab.com/CatNovember]]
